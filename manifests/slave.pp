file { "/etc/hosts":
	ensure => present,
	source => "file:///vagrant/files/slave_hosts_file",
}

file { "/etc/puppet/puppet.conf":
	ensure => present,
	source => "file:///vagrant/files/puppet.conf",
}
