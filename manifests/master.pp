package { 'puppet-server':
        ensure => installed,
}

exec { 'puppet_upgrade':
        command => '/bin/puppet resource package puppet ensure=latest',
}

service { 'puppetmaster':
        require => Package['puppet-server'],
        ensure => running,
}

exec { 'firewall_add':
	command => '/bin/firewall-cmd --permanent --zone=public --add-port=8140/tcp',
}

exec { 'firewall_reload':
	command => '/bin/firewall-cmd --reload',
}

file { "/etc/puppet/manifests":
    ensure => "directory",
}
